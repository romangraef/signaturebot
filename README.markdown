# Signaturebot
This simple signature bot allows you to sign all your messages if the normal signature isn't enough. Simple setup via JSON and cross-platform via Java.

Setup: either use an release (if i finally figured out how to use curl in order to upload files to gitlab) or clone/download the repository
in the target folder edit the `config.json` and put your own discord token in.
to find out your token go into your discord client and press `Ctrl+Shift+I` click on the arrows pointing right in the upper right corner and then on application.
then find Localstorage -> https://discordapp.com/... and double click the "token" section to get your token. 

TODO: add custom signature.