package de.romjaki.signaturebot;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

public class Main {
    static JDA jda;

    public static void main(String[] args) {
        Config.load();
        try {
            jda = new JDABuilder(AccountType.CLIENT).setToken(Config.TOKEN).addEventListener(new Signer())
                    .buildBlocking();
        } catch (LoginException | InterruptedException e) {
            System.err.println("Failed to login");
            System.exit(1);
        }
    }
}