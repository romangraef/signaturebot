package de.romjaki.signaturebot;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

public class Config {
    private Config() {
    }

    public static String TOKEN = "";

    public static void load() {
        try {
            JSONObject obj = new JSONObject(FileUtils.readFileToString(new File("config.json"), "UTF-8"));
            TOKEN = obj.getString("token");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}