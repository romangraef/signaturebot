package de.romjaki.signaturebot;

import net.dv8tion.jda.core.entities.MessageType;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import static de.romjaki.signaturebot.Main.*;

public class Signer extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        super.onMessageReceived(event);

        if (event.getMessage().getType() != MessageType.DEFAULT) {
            return;
        }
        if (!event.getAuthor().equals(jda.getSelfUser())) {
            return;
        }
        if (!event.getMessage().getEmbeds().isEmpty()) {
            return;
        }

        // Only non-embed text message by the author 
        String rawContent = event.getMessage().getContentRaw();
        String suffix = getSuffix(event.getAuthor());
        if (rawContent.endsWith(suffix)) {
            return;
        }
        if (rawContent.length() + suffix.length() > 2000) {
            return;
        }
        event.getMessage().editMessage(rawContent + suffix).queue();        
    }

    private String getSuffix(User author) {
        return "\nGesendet von " + author.getAsMention();
    }
}